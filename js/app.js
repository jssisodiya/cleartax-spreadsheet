var sortingByIndex = 0;

var mainNode = document.getElementById('root');

function getSheetData() {
	var sheetDataStr = localStorage.getItem('sheet');
	if (!sheetDataStr) {
		return {};
	} else {
		return JSON.parse(localStorage.getItem('sheet'));
	}
}

function setSheetData(data) {
	return localStorage.setItem('sheet', JSON.stringify(data));
}

function getRowsCount() {
	return parseInt(localStorage.getItem('rows'));
}
function setRowsCount(cnt) {
	return localStorage.setItem('rows', cnt);
}

function getColsCount() {
	return parseInt(localStorage.getItem('cols'));
}
function setColsCount(cnt) {
	return localStorage.setItem('cols', cnt);
}

function addRow() {
	setRowsCount(getRowsCount() + 1);
	location.reload();
}

function addCol() {
	setColsCount(getColsCount() + 1);
	location.reload();
}

function createRow() {
	var row = document.createElement('div');
	row.className = 'flex-container';
	return row;
}

function createCol(r, c, colData, isHeader, isSortable, isDeletable) {
	var col = document.createElement('div');
	var className = 'col flex-container flex-v-center';
	var innerDiv = document.createElement('div');
	var closeNode = null;
	if (!isHeader && !isSortable) {
		innerDiv.addEventListener('click', enableEditing);
		innerDiv.addEventListener('blur', saveColData);
	}
	if (isDeletable) {
		closeNode = document.createElement('p');
		closeNode.innerHTML = 'x';
		closeNode.className = 'hidden w3-text-red';
		closeNode.addEventListener('click', isSortable ? deleteCol : deleteRow);
		closeNode.setAttribute('data-id', isSortable ? c : r);
	}
	if (isHeader) {
		innerDiv.setAttribute('data-is-header', true);
		className += ' header';
	}
	if (isSortable) {
		innerDiv.addEventListener('click', sortData);
		innerDiv.setAttribute('data-is-sortable', true);
		className += ' sortable';
	}
	col.className = className;
	innerDiv.setAttribute('data-id', r + '-' + c);
	innerDiv.innerHTML = colData;
	console.log(
		'chec',
		isHeader,
		isSortable,
		c,
		parseInt(localStorage.getItem('sortingIndex'))
	);
	if (isHeader && isSortable && c == localStorage.getItem('sortingIndex')) {
		var sortOrder = localStorage.getItem('sortOrder') || 1;
		console.log('sortOrder', sortOrder);
		innerDiv.innerHTML =
			innerDiv.innerHTML + (sortOrder == 1 ? '&uarr;' : '&darr;');
	}
	col.append(innerDiv);
	if (closeNode) {
		col.append(closeNode);
	}
	return col;
}

function initSheet(rows, cols) {
	var sheetData = getSheetData() || {};

	var row = createRow(0);
	row.append(createCol(0, 0, '', true));
	for (j = 1; j <= cols; j++) {
		row.append(createCol(0, j, '' + j, true, true, true));
	}
	mainNode.append(row);
	for (i = 1; i <= rows; i++) {
		var row = createRow(i);
		row.append(createCol(i, j, '' + i, true, false, true));
		for (j = 1; j <= cols; j++) {
			var colData = sheetData[i + '-' + j] || '';
			row.append(createCol(i, j, colData));
		}
		mainNode.append(row);
	}
}

function enableEditing(e) {
	e.target.contentEditable = 'true';
}

function saveColData(e) {
	var el = e.target;
	var sheetData = getSheetData() || {};
	sheetData[el.getAttribute('data-id')] = el.innerHTML;
	setSheetData(sheetData);
	el.contentEditable = 'false';
}

function sortData(e) {
	var el = e.target;
	var id = el.getAttribute('data-id');
	var isSortable = el.getAttribute('data-is-sortable');
	var sortOrder = localStorage.getItem('sortOrder');
	if (sortOrder) {
		sortOrder = -1 * parseInt(sortOrder);
	} else {
		sortOrder = 1;
	}
	localStorage.setItem('sortOrder', sortOrder);
	localStorage.setItem('sortingIndex', parseInt(id.split('-')[1]));

	sortingByIndex = parseInt(id.split('-')[1]) - 1;
	var sheetData = getSheetData();
	var twoDData = get2DData(sheetData);
	twoDData.sort(sortFunction);
	var sortedSheetData = {};
	for (i = 1; i <= twoDData.length; i++) {
		var arr = twoDData[i - 1];
		for (j = 1; j <= arr.length; j++) {
			sortedSheetData[i + '-' + j] = twoDData[i - 1][j - 1];
		}
	}
	setSheetData(sortedSheetData);
	location.reload();
}

function get2DData(sheetData) {
	var twoDData = [];
	for (i = 1; i <= ROWS; i++) {
		var arr = [];
		for (j = 1; j <= COLS; j++) {
			arr.push(sheetData[i + '-' + j] || '');
		}
		twoDData.push(arr);
	}
	return twoDData;
}

// Sort
// takes the sorting index and order from LocalStorage
function sortFunction(a, b) {
	var sortOrder = parseInt(localStorage.getItem('sortOrder') || 1);
	var firstVal = a[sortingByIndex];
	var secVal = b[sortingByIndex];
	if (firstVal === secVal) {
		return 0;
	} else {
		return firstVal < secVal ? sortOrder * -1 : sortOrder * 1;
	}
}

// Delete the values as per the index, decrease rows count by 1 and save it back in local storage
function deleteRow(e) {
	var el = e.target;
	var index = parseInt(el.getAttribute('data-id'));
	var rows = getRowsCount();
	var cols = getColsCount();
	var sheetData = getSheetData();
	var twoDData = get2DData(sheetData);
	twoDData.splice(index - 1, 1);
	var newSheetData = {};
	for (i = 1; i <= twoDData.length; i++) {
		var arr = twoDData[i - 1];
		for (j = 1; j <= arr.length; j++) {
			newSheetData[i + '-' + j] = twoDData[i - 1][j - 1];
		}
	}
	setRowsCount(rows - 1);
	setSheetData(newSheetData);
	location.reload();
}

// Delete the values as per the index, decrease columns count by 1 and save it back in local storage
function deleteCol(e) {
	var el = e.target;
	var index = parseInt(el.getAttribute('data-id'));
	var rows = getRowsCount();
	var cols = getColsCount();
	var sheetData = getSheetData();
	var twoDData = get2DData(sheetData);
	for (var i = 0; i < twoDData.length; i++) {
		twoDData[i].splice(index - 1, 1);
	}
	var newSheetData = {};
	for (i = 1; i <= twoDData.length; i++) {
		var arr = twoDData[i - 1];
		for (j = 1; j <= arr.length; j++) {
			newSheetData[i + '-' + j] = twoDData[i - 1][j - 1];
		}
	}
	setColsCount(cols - 1);
	setSheetData(newSheetData);
	location.reload();
}

// Fetch number of rows and columns from LocalStorage.
// Default values - 15 x 10
var ROWS = localStorage.getItem('rows') || 15;
localStorage.setItem('rows', ROWS);
var COLS = localStorage.getItem('cols') || 10;
localStorage.setItem('cols', COLS);

// Create table cells using javascript
initSheet(ROWS, COLS);
